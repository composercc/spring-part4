package com.jin.myblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jin.myblog.entity.Article;
import com.jin.myblog.mapper.ArticleMapper;
import com.jin.myblog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liJin
 * @date 2021/1/6 17:18
 * @desc
 */
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleMapper articleMapper;

    @Override
    public IPage<Article> listArticle(IPage iPage) {
        QueryWrapper<Article> qw = new QueryWrapper<>();
        //qw.like("title", "");
        return articleMapper.selectPage(iPage, qw);
    }
}
