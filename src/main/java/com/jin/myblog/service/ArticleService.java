package com.jin.myblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jin.myblog.entity.Article;

import java.util.List;

/**
 * @author liJin
 * @date 2021/1/6 17:17
 * @desc
 */
public interface ArticleService {

    IPage<Article>  listArticle(IPage iPage);

}
