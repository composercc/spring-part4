package com.jin.myblog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jin.myblog.entity.Article;
import com.jin.myblog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author liJin
 * @date 2021/1/6 16:31
 * @desc
 */
@Controller
public class BlogController {
    
    @Autowired
    ArticleService articleService;


    @RequestMapping("index")
    public String index(Model model,
                        @RequestParam(name = "page", required = false, defaultValue = "1") String page,
                        @RequestParam(name = "pageSize", required = false, defaultValue = "2") String pageSize) {
        int current = 1;
        int size = 2;
        try {
            current = Integer.parseInt(page);
            size = Integer.parseInt(pageSize);
        } catch (Exception e) {

        }
        IPage<Article> iPage = new Page<>(current, size);
        articleService.listArticle(iPage);
        model.addAttribute("articles",iPage.getRecords());
        model.addAttribute("total",iPage.getTotal());
        model.addAttribute("current",iPage.getCurrent());
        model.addAttribute("size",iPage.getSize());
        return "client/index";
    }

}
