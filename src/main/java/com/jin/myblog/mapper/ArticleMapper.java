package com.jin.myblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jin.myblog.entity.Article;

/**
 * @author liJin
 * @date 2021/1/6 17:12
 * @desc
 */
public interface ArticleMapper extends BaseMapper<Article> {
}
